/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * PDFAnnotationExtractor is Copyright (c) 2020-2020 Nathan Yu.
 *
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * For commercial licensing, please contact nathan17724@gmail.com.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "AnnotationUtil.h"
#include "CodecConvertor.h"

pdf_page *AnnotationUtil::get_pdf_page(fz_context *pdfContext, pdf_document *pdfDocument, const int pageNumber)
{
	if (pdfContext == NULL || pdfDocument == NULL || pageNumber < 0)
	{
		return NULL;
	}

	pdf_page *pdfpage = pdf_load_page(pdfContext, pdfDocument, pageNumber);

	return pdfpage;
}

pdf_annot *AnnotationUtil::get_page_first_annotation(fz_context *pdfContext, pdf_page *pdfPage)
{
	if (pdfContext == NULL || pdfPage == NULL)
	{
		return NULL;
	}

	pdf_annot *firstAnnotation = pdf_first_annot(pdfContext, pdfPage);

	return firstAnnotation;
}

std::wstring AnnotationUtil::get_pdf_annotation_type(fz_context *pdfContext, pdf_annot *annotation)
{
	if (pdfContext == NULL || annotation == NULL)
	{
		return L"Unknow";
	}

	enum pdf_annot_type annotationType = pdf_annot_type(pdfContext, annotation);
	std::string annotationTypeName = pdf_string_from_annot_type(pdfContext, annotationType);
	std::wstring typeName = CodecConvertor::utf8_to_wstring(annotationTypeName);

	return typeName;
}

std::wstring AnnotationUtil::get_pdf_annotation_content(fz_context *pdfContext, pdf_annot *annotation)
{
	if (pdfContext == NULL || annotation == NULL)
	{
		return L"";
	}

	std::string utf8Content = pdf_annot_contents(pdfContext, annotation);
	std::wstring content = CodecConvertor::utf8_to_wstring(utf8Content);

	content = CodecConvertor::replace_trailing_to_windows_style(content, L"\r");

	return content;
}

fz_context *AnnotationUtil::create_pdf_context()
{
	fz_context *pdfContext = fz_new_context(NULL, NULL, FZ_STORE_DEFAULT);

	return pdfContext;
}

pdf_document *AnnotationUtil::load_pdf_file(std::wstring const& pdfFilePath, fz_context *pdfContext)
{
	if (pdfContext == NULL)
	{
		return NULL;
	}

	if (pdfFilePath.empty())
	{
		return NULL;
	}

	std::string pdfPath = CodecConvertor::wstring_to_ansi(pdfFilePath);
	pdf_document *pdfDocument = pdf_open_document(pdfContext, pdfPath.c_str());

	return pdfDocument;
}

int AnnotationUtil::get_pdf_page_count(fz_context *pdfContext, pdf_document *pdfDocument)
{
	if (pdfContext == NULL || pdfDocument == NULL)
	{
		return 0;
	}

	return pdf_count_pages(pdfContext, pdfDocument);
}

std::vector<std::tuple<int, int, std::wstring>> AnnotationUtil::get_page_all_annotation_content(fz_context *pdfContext, pdf_document *pdfDocument, const int pageNumber)
{
	std::vector<std::tuple<int, int, std::wstring>> annotationContentList;

	if (pdfContext == NULL || pdfDocument == NULL || pageNumber < 0)
	{
		return annotationContentList;
	}

	pdf_page *pdfPage = get_pdf_page(pdfContext, pdfDocument, pageNumber);

	if (pdfPage == NULL)
	{
		return annotationContentList;
	}

	int indexNumber = 0;
	std::wstring annotationContent;
	std::tuple<int, int, std::wstring> annotationItem;
	pdf_annot *firstAnnotation = get_page_first_annotation(pdfContext, pdfPage);

	while (firstAnnotation != NULL)
	{
		annotationContent = get_pdf_annotation_content(pdfContext, firstAnnotation);
		std::wstring typeName = get_pdf_annotation_type(pdfContext, firstAnnotation);

		if (annotationContent.empty())
		{
			firstAnnotation = firstAnnotation->next;
			continue;
		}

		indexNumber++;
		annotationItem = {pageNumber + 1, indexNumber, annotationContent};
		annotationContentList.push_back(annotationItem);
		firstAnnotation = firstAnnotation->next;
	}

	return annotationContentList;
}

void AnnotationUtil::close_pdf_document(fz_context *pdfContext, pdf_document *pdfDocument)
{
	if (pdfContext == NULL || pdfDocument == NULL)
	{
		return;
	}

	pdf_drop_document(pdfContext, pdfDocument);
	fz_drop_context(pdfContext);
}