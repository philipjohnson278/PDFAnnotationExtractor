/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * PDFAnnotationExtractor is Copyright (c) 2020-2020 Nathan Yu.
 *
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * For commercial licensing, please contact nathan17724@gmail.com.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <fstream>

#include <urlmon.h>

#include "FileUtil.h"

bool FileUtil::check_file_exists(const std::wstring& filePath)
{
	DWORD fileAttrib = ::GetFileAttributes(filePath.c_str());

	if (fileAttrib != INVALID_FILE_ATTRIBUTES && !(fileAttrib & FILE_ATTRIBUTE_DIRECTORY))
	{
		return true;
	}

	return false;
}

bool FileUtil::check_directory_exists(const std::wstring& directoryPath)
{
	DWORD directoryAttrib = ::GetFileAttributes(directoryPath.c_str());

	if (directoryAttrib != INVALID_FILE_ATTRIBUTES && (directoryAttrib & FILE_ATTRIBUTE_DIRECTORY))
	{
		return true;
	}

	return false;
}

std::wstring FileUtil::get_file_name_extension(const std::wstring& filePath)
{
	std::wstring::size_type position = filePath.find_last_of(L".");
	std::wstring pdfFileExtension = L"";

	if (position != std::wstring::npos)
	{
		pdfFileExtension = filePath.substr(position);
	}

	return pdfFileExtension;
}

std::wstring FileUtil::detect_file_mime_type(const std::wstring& filePath)
{
	std::wstring mimeType = L"";
	std::ifstream inStream(filePath, std::ios::in | std::ios::binary);

	if (!inStream.is_open())
	{
		return mimeType;
	}

	char buffer[256] = { 0 };

	inStream.read(buffer, sizeof(buffer));
	inStream.close();

	LPWSTR mimeBuffer = NULL;
	HRESULT hresult = ::FindMimeFromData(NULL, NULL, buffer, sizeof(buffer), NULL, FMFD_DEFAULT, &mimeBuffer, 0);

	if (hresult == S_OK) {
		mimeType = mimeBuffer;
		::CoTaskMemFree(mimeBuffer);
	}

	return mimeType;
}

std::wstring FileUtil::get_file_absolute_path(const std::wstring& filePath)
{
	wchar_t buffer[MAX_PATH] = {0};
	std::wstring absolutePath = filePath;

	::GetModuleFileName(NULL, buffer, MAX_PATH);

	std::wstring currentDirectoryPath = buffer;
	std::wstring::size_type position = currentDirectoryPath.find_last_of(L"\\/");

	currentDirectoryPath = currentDirectoryPath.substr(0, position);

	if (absolutePath.empty())
	{
		return currentDirectoryPath;
	}

	if (absolutePath.find(L":\\") != 1)
	{

		if (absolutePath.find(L"\\") == 0)
		{
			if (absolutePath.find(L"\\\\") != 0)
			{
				absolutePath = currentDirectoryPath.substr(0, 2) + absolutePath;
			}
		}
		else
		{
			if (absolutePath.find(L".\\") == 0)
			{
				absolutePath = absolutePath.substr(2);
			}

			absolutePath = currentDirectoryPath + L"\\" + absolutePath;
		}
	}

	return absolutePath;
}