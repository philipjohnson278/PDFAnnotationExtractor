/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * PDFAnnotationExtractor is Copyright (c) 2020-2020 Nathan Yu.
 *
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * For commercial licensing, please contact nathan17724@gmail.com.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include "AnnotationUtil.h"
#include "CodecConvertor.h"
#include "FileUtil.h"
#include "XlsExportor.h"

XlsExportor::XlsExportor(const std::wstring& pdfFilePath, const i18nMessage *localeMessage, const HWND hwndParent) : pdfFilePath(pdfFilePath)
{
	parentDirectory.clear();
	this->localeMessage = (i18nMessage *)localeMessage;
	this->hwndParent = hwndParent;

	if (localeMessage == NULL)
	{
		::MessageBox(NULL, L"Application Initialization Error!", L"!!!ERROR!!!", MB_ICONSTOP | MB_OK);
		exit(0);
	}

	if (pdfFilePath.empty())
	{
		::MessageBox(hwndParent, this->localeMessage->get_i18n_message(L"app_tips_no_file_text").c_str(), this->localeMessage->get_i18n_message(L"app_msgbox_tips_caption_text").c_str(), MB_ICONINFORMATION | MB_OK);
		exit(0);
	}

	std::wstring::size_type position = pdfFilePath.find_last_of(L"\\/");

	if (position != std::wstring::npos)
	{
		parentDirectory = pdfFilePath.substr(0, position);
	}
	else
	{
		::MessageBox(hwndParent, this->localeMessage->get_i18n_message(L"app_error_open_directory_text").c_str(), this->localeMessage->get_i18n_message(L"app_msgbox_error_caption_text").c_str(), MB_ICONSTOP | MB_OK);
		exit(0);
	}

	if (!FileUtil::check_directory_exists(parentDirectory))
	{
		::MessageBox(hwndParent, this->localeMessage->get_i18n_message(L"app_error_open_directory_text").c_str(), this->localeMessage->get_i18n_message(L"app_msgbox_error_caption_text").c_str(), MB_ICONSTOP | MB_OK);
		exit(0);
	}
}

std::wstring XlsExportor::get_default_export_path()
{
	if (parentDirectory.empty())
	{
		::MessageBox(hwndParent, localeMessage->get_i18n_message(L"app_error_open_directory_text").c_str(), localeMessage->get_i18n_message(L"app_msgbox_error_caption_text").c_str(), MB_ICONSTOP | MB_OK);

		return L"";
	}

	std::wstring pdfFileExtension = FileUtil::get_file_name_extension(pdfFilePath.c_str());
	int pdfNameLength = pdfFilePath.size() - parentDirectory.size() - pdfFileExtension.size() - 1;
	std::wstring annotationFilePathNoFileExt = parentDirectory + L"\\" + pdfFilePath.substr(parentDirectory.size() + 1, pdfNameLength);
	std::wstring annotationFilePath = parentDirectory + L"\\Annotation.xls";

	if (pdfNameLength > 0)
	{
		annotationFilePath = annotationFilePathNoFileExt + L" - Annotation.xls";

		if (annotationFilePath.size() >= MAX_PATH)
		{
			annotationFilePath = annotationFilePathNoFileExt + L".xls";
		}
	}

	if (annotationFilePath.size() >= MAX_PATH)
	{
		::MessageBox(hwndParent, this->localeMessage->get_i18n_message(L"app_tips_filename_too_long_text").c_str(), this->localeMessage->get_i18n_message(L"app_msgbox_tips_caption_text").c_str(), MB_ICONINFORMATION | MB_OK);
		return L"";
	}

	return annotationFilePath;
}

void XlsExportor::write_xls_row(const int xlsRowIndex, std::vector<std::tuple<int, int, std::wstring>> const annotationItemList, xlslib_core::worksheet *xlsSheet, xlslib_core::xf_t *xlsFormat)
{
	if (annotationItemList.empty() || xlsSheet == NULL || xlsFormat == NULL)
	{
		return;
	}

	std::tuple<int, int, std::wstring> annotationItem;

	for (size_t i = 0; i < annotationItemList.size(); i++)
	{
		annotationItem = annotationItemList[i];

		if (std::get<2>(annotationItem).empty())
		{
			continue;
		}

		xlsSheet->label(xlsRowIndex + i, 0, std::to_string(std::get<0>(annotationItem)), xlsFormat);
		xlsSheet->label(xlsRowIndex + i, 1, std::to_string(std::get<1>(annotationItem)), xlsFormat);
		xlsSheet->label(xlsRowIndex + i, 2, std::get<2>(annotationItem), xlsFormat);
	}
}

void XlsExportor::export_annotation(const std::wstring& xlsFileExportPath)
{
	if (xlsFileExportPath.empty())
	{
		::MessageBox(hwndParent, localeMessage->get_i18n_message(L"app_error_export_failure_text").c_str(), localeMessage->get_i18n_message(L"app_msgbox_error_caption_text").c_str(), MB_ICONSTOP | MB_OK);
		return;
	}

	fz_context *pdfContext = AnnotationUtil::create_pdf_context();

	if (pdfContext == NULL)
	{
		::MessageBox(hwndParent, localeMessage->get_i18n_message(L"app_error_export_failure_text").c_str(), localeMessage->get_i18n_message(L"app_msgbox_error_caption_text").c_str(), MB_ICONSTOP | MB_OK);
		return;
	}

	pdf_document *pdfDocument = AnnotationUtil::load_pdf_file(pdfFilePath, pdfContext);

	if (pdfDocument == NULL)
	{
		::MessageBox(hwndParent, localeMessage->get_i18n_message(L"app_error_export_failure_text").c_str(), localeMessage->get_i18n_message(L"app_msgbox_error_caption_text").c_str(), MB_ICONSTOP | MB_OK);
		return;
	}

	int pdfPageCount = AnnotationUtil::get_pdf_page_count(pdfContext, pdfDocument);

	if (pdfPageCount <= 0)
	{
		::MessageBox(hwndParent, localeMessage->get_i18n_message(L"app_error_export_failure_text").c_str(), localeMessage->get_i18n_message(L"app_msgbox_error_caption_text").c_str(), MB_ICONSTOP | MB_OK);
		return;
	}

	bool isFileExists = FileUtil::check_file_exists(xlsFileExportPath);

	if (isFileExists)
	{
		int comfirm = ::MessageBox(hwndParent, (L"\"" + xlsFileExportPath + L"\"" + localeMessage->get_i18n_message(L"app_comfirm_overwrite_file_text")).c_str(), localeMessage->get_i18n_message(L"app_msgbox_comfirm_caption_text").c_str(), MB_ICONQUESTION | MB_OKCANCEL);

		if (comfirm != IDOK)
		{
			return;
		}

		::SetFileAttributes(xlsFileExportPath.c_str(), GetFileAttributes(xlsFileExportPath.c_str()) & ~FILE_ATTRIBUTE_READONLY);
	}

	xlslib_core::workbook xlsDocument;
	xlslib_core::worksheet *xlsSheet = xlsDocument.sheet(L"Annotation");
	xlslib_core::xf_t *xlsFormat = xlsDocument.xformat();

	if (xlsSheet == NULL || xlsFormat == NULL)
	{
		::MessageBox(hwndParent, localeMessage->get_i18n_message(L"app_error_export_failure_text").c_str(), localeMessage->get_i18n_message(L"app_msgbox_error_caption_text").c_str(), MB_ICONSTOP | MB_OK);
		return;
	}

	xlsSheet->label(0, 0, localeMessage->get_i18n_message(L"xls_column_page_title_text"), xlsFormat);
	xlsSheet->label(0, 1, localeMessage->get_i18n_message(L"xls_column_index_title_text"), xlsFormat);
	xlsSheet->label(0, 2, localeMessage->get_i18n_message(L"xls_column_content_title_text"), xlsFormat);

	int xlsRowIndex = 1;
	std::vector<std::tuple<int, int, std::wstring>> annotationItemList;

	for (int i = 0; i < pdfPageCount; i++)
	{
		annotationItemList = AnnotationUtil::get_page_all_annotation_content(pdfContext, pdfDocument, i);
		write_xls_row(xlsRowIndex, annotationItemList, xlsSheet, xlsFormat);
		xlsRowIndex = xlsRowIndex + annotationItemList.size();
		annotationItemList.clear();
	}

	xlsDocument.Dump(CodecConvertor::wide_to_narrow(xlsFileExportPath));
	AnnotationUtil::close_pdf_document(pdfContext, pdfDocument);

	::MessageBox(hwndParent, localeMessage->get_i18n_message(L"app_tips_export_finish_text").c_str(), localeMessage->get_i18n_message(L"app_msgbox_tips_caption_text").c_str(), MB_ICONINFORMATION | MB_OK);
}