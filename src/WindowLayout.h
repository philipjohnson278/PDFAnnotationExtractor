/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * PDFAnnotationExtractor is Copyright (c) 2020-2020 Nathan Yu.
 *
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * For commercial licensing, please contact nathan17724@gmail.com.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#pragma once

#include <Windows.h>

#include "i18nMessage.h"

#define	BOX_ID_FILE_PATH	7224
#define	BTN_ID_SELECT_FILE	7225
#define	BTN_ID_EXPORT_FILE	7226

class WindowLayout
{
public:
	WindowLayout(const i18nMessage *localeMessage, const HWND hwndParent, const HINSTANCE hInstance);
	virtual ~WindowLayout();

	HWND create_input_box(const int xPos, const int yPos, const int width, const int height);
	HWND create_select_button(const int xPos, const int yPos, const int width, const int height);
	HWND create_export_button(const int xPos, const int yPos, const int width, const int height);
	void set_input_box_text_vertical_center(const HWND hwndEditBox);
	void center_window_on_screen(const HWND hwndEditBox);

private:
	WindowLayout() {}

	HWND hwndParent;
	HINSTANCE hInstance;
	HMODULE hmoduleRichEdit;
	i18nMessage *localeMessage;
};