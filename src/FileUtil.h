/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * PDFAnnotationExtractor is Copyright (c) 2020-2020 Nathan Yu.
 *
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * For commercial licensing, please contact nathan17724@gmail.com.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#pragma once

#include <string>

class FileUtil
{
public:
	static bool check_file_exists(const std::wstring& filePath);
	static bool check_directory_exists(const std::wstring& directoryPath);
	static std::wstring get_file_name_extension(const std::wstring& filePath);
	static std::wstring detect_file_mime_type(const std::wstring& filePath);
	static std::wstring get_file_absolute_path(const std::wstring& filePath);

private:
	FileUtil() {}
	virtual ~FileUtil() {}
};