/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * PDFAnnotationExtractor is Copyright (c) 2020-2020 Nathan Yu.
 *
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * For commercial licensing, please contact nathan17724@gmail.com.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#pragma once

#include <string>

#include <Windows.h>
#include <Shlobj.h>

#include "i18nMessage.h"

class EventHandler
{
public:
	EventHandler(const i18nMessage *localeMessage, const HWND hwndParent);
	virtual ~EventHandler();

	bool handle_select_pdf_file_event(HWND hwndEditBox);
	bool handle_export_annotation_event(HWND hwndEditBox);

private:
	EventHandler() {}

	wchar_t *filterBuffer;
	wchar_t pdfFilePath[MAX_PATH] = {0};
	i18nMessage *localeMessage;
	HWND hwndParent;
	OPENFILENAME pdfFileName = {0};

	void init_select_file_filter();
	void init_select_file_data();
	void show_input_box_error_message();
	std::wstring check_selected_pdf_file_path(HWND hwndEditBox);
	void set_selected_pdf_file_path(const std::wstring& pdf_file_path, HWND hwndEditBox);
};