/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * PDFAnnotationExtractor is Copyright (c) 2020-2020 Nathan Yu.
 *
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * For commercial licensing, please contact nathan17724@gmail.com.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#pragma once

#include <string>

class CodecConvertor
{
public:
	static std::string wide_to_narrow(std::wstring const& wideString);
	static std::wstring narrow_to_wide(std::string const& narrowString);
	static std::string wstring_to_ansi(std::wstring const& wString);
	static std::wstring ansi_to_wstring(std::string const& ansiString);
	static std::wstring utf8_to_wstring(const std::string& utf8String);
	static std::string wstring_to_utf8(const std::wstring& wString);
	static std::wstring replace_trailing_to_windows_style(const std::wstring& targetWString, const std::wstring& oldTrailing);

private:
	CodecConvertor() {}
	virtual ~CodecConvertor() {}
};