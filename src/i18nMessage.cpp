/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * PDFAnnotationExtractor is Copyright (c) 2020-2020 Nathan Yu.
 *
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * For commercial licensing, please contact nathan17724@gmail.com.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <string>

#include <windows.h>
#include <Winnls.h>
#include <AtlBase.h>
#include <atlconv.h>

#include "i18nMessage.h"

// FIXME: This is a sily way to handle I18N.
// TODO:  Improve the I18N handling, such as loading I18N messages from an exten file.

struct LocaleMessage
{
	static std::wstring app_caption_text;
	static std::wstring app_btn_choice_text;
	static std::wstring app_btn_export_text;
	static std::wstring app_diag_caption_text;
	static std::wstring app_diag_file_filter_text;
	static std::wstring app_msgbox_error_caption_text;
	static std::wstring app_msgbox_tips_caption_text;
	static std::wstring app_msgbox_comfirm_caption_text;
	static std::wstring app_error_inititalization_text;
	static std::wstring app_error_file_path_text;
	static std::wstring app_error_pdf_document_text;
	static std::wstring app_error_open_directory_text;
	static std::wstring app_error_write_file_text;
	static std::wstring app_error_export_failure_text;
	static std::wstring app_comfirm_overwrite_file_text;
	static std::wstring app_tips_no_file_text;
	static std::wstring app_tips_filename_too_long_text;
	static std::wstring app_tips_file_exists_text;
	static std::wstring app_tips_file_correct_text;
	static std::wstring app_tips_export_finish_text;
	static std::wstring xls_column_page_title_text;
	static std::wstring xls_column_index_title_text;
	static std::wstring xls_column_content_title_text;
};

struct LocaleCHSMessage
{
	static std::wstring app_caption_text;
	static std::wstring app_btn_choice_text;
	static std::wstring app_btn_export_text;
	static std::wstring app_diag_caption_text;
	static std::wstring app_diag_file_filter_text;
	static std::wstring app_msgbox_error_caption_text;
	static std::wstring app_msgbox_tips_caption_text;
	static std::wstring app_msgbox_comfirm_caption_text;
	static std::wstring app_error_inititalization_text;
	static std::wstring app_error_file_path_text;
	static std::wstring app_error_pdf_document_text;
	static std::wstring app_error_open_directory_text;
	static std::wstring app_error_write_file_text;
	static std::wstring app_error_export_failure_text;
	static std::wstring app_comfirm_overwrite_file_text;
	static std::wstring app_tips_no_file_text;
	static std::wstring app_tips_filename_too_long_text;
	static std::wstring app_tips_file_exists_text;
	static std::wstring app_tips_file_correct_text;
	static std::wstring app_tips_export_finish_text;
	static std::wstring xls_column_page_title_text;
	static std::wstring xls_column_index_title_text;
	static std::wstring xls_column_content_title_text;
};

struct LocaleCHTMessage
{
	static std::wstring app_caption_text;
	static std::wstring app_btn_choice_text;
	static std::wstring app_btn_export_text;
	static std::wstring app_diag_caption_text;
	static std::wstring app_diag_file_filter_text;
	static std::wstring app_msgbox_error_caption_text;
	static std::wstring app_msgbox_tips_caption_text;
	static std::wstring app_msgbox_comfirm_caption_text;
	static std::wstring app_error_inititalization_text;
	static std::wstring app_error_file_path_text;
	static std::wstring app_error_pdf_document_text;
	static std::wstring app_error_open_directory_text;
	static std::wstring app_error_write_file_text;
	static std::wstring app_error_export_failure_text;
	static std::wstring app_comfirm_overwrite_file_text;
	static std::wstring app_tips_no_file_text;
	static std::wstring app_tips_filename_too_long_text;
	static std::wstring app_tips_file_exists_text;
	static std::wstring app_tips_file_correct_text;
	static std::wstring app_tips_export_finish_text;
	static std::wstring xls_column_page_title_text;
	static std::wstring xls_column_index_title_text;
	static std::wstring xls_column_content_title_text;
};

class LocaleInfo
{
public:
	static std::string ansiLocaleName;
	static std::wstring wideLocaleName;

private:
	LocaleInfo() {}
	virtual ~LocaleInfo() {}

	static std::string get_ansi_locale_name();
	static std::wstring get_wide_locale_name();
};

std::string LocaleInfo::ansiLocaleName = get_ansi_locale_name();
std::wstring LocaleInfo::wideLocaleName = get_wide_locale_name();

std::wstring LocaleMessage::app_caption_text = L"PDF Annotation Extractor";
std::wstring LocaleMessage::app_btn_choice_text = L"Select File";
std::wstring LocaleMessage::app_btn_export_text = L"Export";
std::wstring LocaleMessage::app_diag_caption_text = L"Please Select A PDF File";
std::wstring LocaleMessage::app_diag_file_filter_text = L"PDF File (*.pdf)";
std::wstring LocaleMessage::app_msgbox_error_caption_text = L"!!!ERROR!!!";
std::wstring LocaleMessage::app_msgbox_tips_caption_text = L"Tips";
std::wstring LocaleMessage::app_msgbox_comfirm_caption_text = L"Please Comfirm!";
std::wstring LocaleMessage::app_error_inititalization_text = L"Application Initialization Error!";
std::wstring LocaleMessage::app_error_file_path_text = L"PDF File Path Not Valid!";
std::wstring LocaleMessage::app_error_pdf_document_text = L"Can Not Load PDF File!";
std::wstring LocaleMessage::app_error_open_directory_text = L"Can Not Access Directory For Exporting!";
std::wstring LocaleMessage::app_error_write_file_text = L"Can Not Write File For Exporting!";
std::wstring LocaleMessage::app_error_export_failure_text = L"Export Failure!";
std::wstring LocaleMessage::app_comfirm_overwrite_file_text = L" Already Exists, Should Overwrite It?";
std::wstring LocaleMessage::app_tips_no_file_text = L"No PDF File Found, Please Select One!";
std::wstring LocaleMessage::app_tips_file_exists_text = L"Please Check Selected PDF File Exists Or Not!";
std::wstring LocaleMessage::app_tips_file_correct_text = L"Please Check Selected PDF File Valid Or Not!";
std::wstring LocaleMessage::app_tips_filename_too_long_text = L"The Selected PDF File Path Too Long!";
std::wstring LocaleMessage::app_tips_export_finish_text = L"Export Finish!";
std::wstring LocaleMessage::xls_column_page_title_text = L"Page Number";
std::wstring LocaleMessage::xls_column_index_title_text = L"Index In Page";
std::wstring LocaleMessage::xls_column_content_title_text = L"Annotation Content";

std::wstring LocaleCHSMessage::app_caption_text = L"PDF注释提取器";
std::wstring LocaleCHSMessage::app_btn_choice_text = L"选择PDF文件";
std::wstring LocaleCHSMessage::app_btn_export_text = L"导出注释";
std::wstring LocaleCHSMessage::app_diag_caption_text = L"请选取PDF文件";
std::wstring LocaleCHSMessage::app_diag_file_filter_text = L"PDF 文件 (*.pdf)";
std::wstring LocaleCHSMessage::app_msgbox_error_caption_text = L"！！！错误！！！";
std::wstring LocaleCHSMessage::app_msgbox_tips_caption_text = L"提示";
std::wstring LocaleCHSMessage::app_msgbox_comfirm_caption_text = L"请确认！";
std::wstring LocaleCHSMessage::app_error_inititalization_text = L"应用程序初始化失败！";
std::wstring LocaleCHSMessage::app_error_file_path_text = L"无法确定PDF文件的路径！";
std::wstring LocaleCHSMessage::app_error_pdf_document_text = L"无法加载PDF文档！";
std::wstring LocaleCHSMessage::app_error_open_directory_text = L"无法访问导出目录！";
std::wstring LocaleCHSMessage::app_error_write_file_text = L"无法导出数据到文件！";
std::wstring LocaleCHSMessage::app_error_export_failure_text = L"导出失败！";
std::wstring LocaleCHSMessage::app_comfirm_overwrite_file_text = L" 已经存在，是否覆盖？";
std::wstring LocaleCHSMessage::app_tips_no_file_text = L"请选取PDF文件！";
std::wstring LocaleCHSMessage::app_tips_file_exists_text = L"请检查所选取的PDF文件是否存在！";
std::wstring LocaleCHSMessage::app_tips_file_correct_text = L"请检查所选取的PDF文件是否有效！";
std::wstring LocaleCHSMessage::app_tips_filename_too_long_text = L"所选取的PDF文件路径过长！";
std::wstring LocaleCHSMessage::app_tips_export_finish_text = L"导出完成！";
std::wstring LocaleCHSMessage::xls_column_page_title_text = L"所在页";
std::wstring LocaleCHSMessage::xls_column_index_title_text = L"在该页中的序号";
std::wstring LocaleCHSMessage::xls_column_content_title_text = L"注释内容";

std::wstring LocaleCHTMessage::app_caption_text = L"PDF註釋提取器";
std::wstring LocaleCHTMessage::app_btn_choice_text = L"選擇PDF文件";
std::wstring LocaleCHTMessage::app_btn_export_text = L"導出註釋";
std::wstring LocaleCHTMessage::app_diag_caption_text = L"請選取PDF文件";
std::wstring LocaleCHTMessage::app_diag_file_filter_text = L"PDF 文件 (*.pdf)";
std::wstring LocaleCHTMessage::app_msgbox_error_caption_text = L"！！！錯誤！！！";
std::wstring LocaleCHTMessage::app_msgbox_tips_caption_text = L"提示";
std::wstring LocaleCHTMessage::app_msgbox_comfirm_caption_text = L"請確認！";
std::wstring LocaleCHTMessage::app_error_inititalization_text = L"應用程序初始化失敗！";
std::wstring LocaleCHTMessage::app_error_file_path_text = L"無法確定PDF文件的路徑！";
std::wstring LocaleCHTMessage::app_error_pdf_document_text = L"無法加載PDF文檔！";
std::wstring LocaleCHTMessage::app_error_open_directory_text = L"無法訪問導出目錄！";
std::wstring LocaleCHTMessage::app_error_write_file_text = L"無法導出數據到文件！";
std::wstring LocaleCHTMessage::app_error_export_failure_text = L"導出失敗！";
std::wstring LocaleCHTMessage::app_comfirm_overwrite_file_text = L" 已經存在，是否覆蓋？";
std::wstring LocaleCHTMessage::app_tips_no_file_text = L"請選取PDF文件！";
std::wstring LocaleCHTMessage::app_tips_file_exists_text = L"請檢查所選取的PDF文件是否存在！";
std::wstring LocaleCHTMessage::app_tips_file_correct_text = L"請檢查所選取的PDF文件是否有效！";
std::wstring LocaleCHTMessage::app_tips_filename_too_long_text = L"所選取的PDF文件路徑過長！";
std::wstring LocaleCHTMessage::app_tips_export_finish_text = L"導出完成！";
std::wstring LocaleCHTMessage::xls_column_page_title_text = L"所在頁";
std::wstring LocaleCHTMessage::xls_column_index_title_text = L"在該頁中的序號";
std::wstring LocaleCHTMessage::xls_column_content_title_text = L"註釋內容";

std::string LocaleInfo::get_ansi_locale_name()
{
	wchar_t localeName[LOCALE_NAME_MAX_LENGTH] = {0};
	int localeNameLength = ::GetSystemDefaultLocaleName(localeName, LOCALE_NAME_MAX_LENGTH);

	if (localeNameLength == 0)
	{
		return "en-US";
	}

	::CW2A cw2a(localeName);
	std::string name = cw2a.m_psz;

	return name;
}

std::wstring LocaleInfo::get_wide_locale_name()
{
	wchar_t localeName[LOCALE_NAME_MAX_LENGTH] = {0};
	int localeNameLength = ::GetSystemDefaultLocaleName(localeName, LOCALE_NAME_MAX_LENGTH);

	if (localeNameLength == 0)
	{
		return L"en-US";
	}

	return localeName;
}

i18nMessage::i18nMessage()
{
	messageMap.clear();
	init_locale_message(get_wide_locale_name());
}

i18nMessage::i18nMessage(const std::wstring *localeName)
{
	if (localeName == NULL)
	{
		return;
	}

	messageMap.clear();
	init_locale_message(*localeName);
}

i18nMessage::~i18nMessage()
{
	messageMap.clear();
}

std::wstring i18nMessage::get_wide_locale_name()
{
	return LocaleInfo::wideLocaleName;
}

std::string i18nMessage::get_ansi_locale_name()
{
	return LocaleInfo::ansiLocaleName;
}

std::wstring i18nMessage::get_i18n_message(const std::wstring& messageName)
{
	return *messageMap[messageName];
}

void i18nMessage::init_locale_message(const std::wstring& localeName)
{
	if (localeName._Equal(L"zh-CN"))
	{
		messageMap[L"app_caption_text"] = &LocaleCHSMessage::app_caption_text;
		messageMap[L"app_btn_choice_text"] = &LocaleCHSMessage::app_btn_choice_text;
		messageMap[L"app_btn_export_text"] = &LocaleCHSMessage::app_btn_export_text;
		messageMap[L"app_diag_caption_text"] = &LocaleCHSMessage::app_diag_caption_text;
		messageMap[L"app_msgbox_error_caption_text"] = &LocaleCHSMessage::app_msgbox_error_caption_text;
		messageMap[L"app_error_inititalization_text"] = &LocaleCHSMessage::app_error_inititalization_text;
		messageMap[L"app_error_file_path_text"] = &LocaleCHSMessage::app_error_file_path_text;
		messageMap[L"app_error_pdf_document_text"] = &LocaleCHSMessage::app_error_pdf_document_text;
		messageMap[L"app_error_open_directory_text"] = &LocaleCHSMessage::app_error_open_directory_text;
		messageMap[L"app_error_write_file_text"] = &LocaleCHSMessage::app_error_write_file_text;
		messageMap[L"app_tips_no_file_text"] = &LocaleCHSMessage::app_tips_no_file_text;
		messageMap[L"app_comfirm_overwrite_file_text"] = &LocaleCHSMessage::app_comfirm_overwrite_file_text;
		messageMap[L"app_tips_file_exists_text"] = &LocaleCHSMessage::app_tips_file_exists_text;
		messageMap[L"app_tips_file_correct_text"] = &LocaleCHSMessage::app_tips_file_correct_text;
		messageMap[L"xls_column_page_title_text"] = &LocaleCHSMessage::xls_column_page_title_text;
		messageMap[L"xls_column_index_title_text"] = &LocaleCHSMessage::xls_column_index_title_text;
		messageMap[L"xls_column_content_title_text"] = &LocaleCHSMessage::xls_column_content_title_text;
		messageMap[L"app_tips_filename_too_long_text"] = &LocaleCHSMessage::app_tips_filename_too_long_text;
		messageMap[L"app_msgbox_tips_caption_text"] = &LocaleCHSMessage::app_msgbox_tips_caption_text;
		messageMap[L"app_msgbox_comfirm_caption_text"] = &LocaleCHSMessage::app_msgbox_comfirm_caption_text;
		messageMap[L"app_diag_file_filter_text"] = &LocaleCHSMessage::app_diag_file_filter_text;
		messageMap[L"app_tips_export_finish_text"] = &LocaleCHSMessage::app_tips_export_finish_text;
		messageMap[L"app_error_export_failure_text"] = &LocaleCHSMessage::app_error_export_failure_text;
	}
	else if (localeName._Equal(L"zh-HK") || localeName._Equal(L"zh-TW"))
	{
		messageMap[L"app_caption_text"] = &LocaleCHTMessage::app_caption_text;
		messageMap[L"app_btn_choice_text"] = &LocaleCHTMessage::app_btn_choice_text;
		messageMap[L"app_btn_export_text"] = &LocaleCHTMessage::app_btn_export_text;
		messageMap[L"app_diag_caption_text"] = &LocaleCHTMessage::app_diag_caption_text;
		messageMap[L"app_msgbox_error_caption_text"] = &LocaleCHTMessage::app_msgbox_error_caption_text;
		messageMap[L"app_error_inititalization_text"] = &LocaleCHTMessage::app_error_inititalization_text;
		messageMap[L"app_error_file_path_text"] = &LocaleCHTMessage::app_error_file_path_text;
		messageMap[L"app_error_pdf_document_text"] = &LocaleCHTMessage::app_error_pdf_document_text;
		messageMap[L"app_error_open_directory_text"] = &LocaleCHTMessage::app_error_open_directory_text;
		messageMap[L"app_error_write_file_text"] = &LocaleCHTMessage::app_error_write_file_text;
		messageMap[L"app_tips_no_file_text"] = &LocaleCHTMessage::app_tips_no_file_text;
		messageMap[L"app_comfirm_overwrite_file_text"] = &LocaleCHTMessage::app_comfirm_overwrite_file_text;
		messageMap[L"app_tips_file_exists_text"] = &LocaleCHTMessage::app_tips_file_exists_text;
		messageMap[L"app_tips_file_correct_text"] = &LocaleCHTMessage::app_tips_file_correct_text;
		messageMap[L"xls_column_page_title_text"] = &LocaleCHTMessage::xls_column_page_title_text;
		messageMap[L"xls_column_index_title_text"] = &LocaleCHTMessage::xls_column_index_title_text;
		messageMap[L"xls_column_content_title_text"] = &LocaleCHTMessage::xls_column_content_title_text;
		messageMap[L"app_tips_filename_too_long_text"] = &LocaleCHTMessage::app_tips_filename_too_long_text;
		messageMap[L"app_msgbox_tips_caption_text"] = &LocaleCHTMessage::app_msgbox_tips_caption_text;
		messageMap[L"app_msgbox_comfirm_caption_text"] = &LocaleCHTMessage::app_msgbox_comfirm_caption_text;
		messageMap[L"app_diag_file_filter_text"] = &LocaleCHTMessage::app_diag_file_filter_text;
		messageMap[L"app_tips_export_finish_text"] = &LocaleCHTMessage::app_tips_export_finish_text;
		messageMap[L"app_error_export_failure_text"] = &LocaleCHTMessage::app_error_export_failure_text;
	}
	else
	{
		messageMap[L"app_caption_text"] = &LocaleMessage::app_caption_text;
		messageMap[L"app_btn_choice_text"] = &LocaleMessage::app_btn_choice_text;
		messageMap[L"app_btn_export_text"] = &LocaleMessage::app_btn_export_text;
		messageMap[L"app_diag_caption_text"] = &LocaleMessage::app_diag_caption_text;
		messageMap[L"app_msgbox_error_caption_text"] = &LocaleMessage::app_msgbox_error_caption_text;
		messageMap[L"app_error_inititalization_text"] = &LocaleMessage::app_error_inititalization_text;
		messageMap[L"app_error_file_path_text"] = &LocaleMessage::app_error_file_path_text;
		messageMap[L"app_error_pdf_document_text"] = &LocaleMessage::app_error_pdf_document_text;
		messageMap[L"app_error_open_directory_text"] = &LocaleMessage::app_error_open_directory_text;
		messageMap[L"app_error_write_file_text"] = &LocaleMessage::app_error_write_file_text;
		messageMap[L"app_tips_no_file_text"] = &LocaleMessage::app_tips_no_file_text;
		messageMap[L"app_comfirm_overwrite_file_text"] = &LocaleMessage::app_comfirm_overwrite_file_text;
		messageMap[L"app_tips_file_exists_text"] = &LocaleMessage::app_tips_file_exists_text;
		messageMap[L"app_tips_file_correct_text"] = &LocaleMessage::app_tips_file_correct_text;
		messageMap[L"xls_column_page_title_text"] = &LocaleMessage::xls_column_page_title_text;
		messageMap[L"xls_column_index_title_text"] = &LocaleMessage::xls_column_index_title_text;
		messageMap[L"xls_column_content_title_text"] = &LocaleMessage::xls_column_content_title_text;
		messageMap[L"app_tips_filename_too_long_text"] = &LocaleMessage::app_tips_filename_too_long_text;
		messageMap[L"app_msgbox_tips_caption_text"] = &LocaleMessage::app_msgbox_tips_caption_text;
		messageMap[L"app_msgbox_comfirm_caption_text"] = &LocaleMessage::app_msgbox_comfirm_caption_text;
		messageMap[L"app_diag_file_filter_text"] = &LocaleMessage::app_diag_file_filter_text;
		messageMap[L"app_tips_export_finish_text"] = &LocaleMessage::app_tips_export_finish_text;
		messageMap[L"app_error_export_failure_text"] = &LocaleMessage::app_error_export_failure_text;
	}
}