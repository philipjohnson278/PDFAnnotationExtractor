/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * PDFAnnotationExtractor is Copyright (c) 2020-2020 Nathan Yu.
 *
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * For commercial licensing, please contact nathan17724@gmail.com.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#pragma once

#include <string>
#include <tuple>
#include <vector>

#include "mupdf/pdf.h"

class AnnotationUtil
{
public:
	static fz_context *create_pdf_context();
	static pdf_document *load_pdf_file(std::wstring const& pdfFilePath, fz_context *pdfContext);
	static int get_pdf_page_count(fz_context *pdfContext, pdf_document *pdfDocument);
	static std::vector<std::tuple<int, int, std::wstring>> get_page_all_annotation_content(fz_context *pdfContext, pdf_document *pdfDocument, const int pageNumber);
	static void close_pdf_document(fz_context *pdfContext, pdf_document *pdfDocument);

private:
	AnnotationUtil() {}
	virtual ~AnnotationUtil() {}

	static pdf_page *get_pdf_page(fz_context *pdfContext, pdf_document *pdfDocument, const int pageNumber);
	static pdf_annot *get_page_first_annotation(fz_context *pdfContext, pdf_page *pdfPage);
	static std::wstring get_pdf_annotation_type(fz_context* pdfContext, pdf_annot* annotation);
	static std::wstring get_pdf_annotation_content(fz_context *pdfContext, pdf_annot *annotation);
};