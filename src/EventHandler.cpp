/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * PDFAnnotationExtractor is Copyright (c) 2020-2020 Nathan Yu.
 *
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * For commercial licensing, please contact nathan17724@gmail.com.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <fstream>

#include <Windows.h>
#include <commdlg.h>
#include <shellapi.h>

#include "EventHandler.h"
#include "FileUtil.h"
#include "XlsExportor.h"

EventHandler::EventHandler(const i18nMessage *localeMessage, const HWND hwndParent)
{
	if (localeMessage == NULL)
	{
		MessageBox(NULL, L"Application Initialization Error!", L"!!!ERROR!!!", MB_ICONSTOP | MB_OK);
		exit(0);
	}

	filterBuffer = NULL;
	this->localeMessage = (i18nMessage *)localeMessage;
	this->hwndParent = (HWND)hwndParent;
	init_select_file_filter();
	init_select_file_data();
}

EventHandler::~EventHandler()
{
	this->localeMessage = NULL;
	this->hwndParent = NULL;

	if (filterBuffer != NULL)
	{
		delete filterBuffer;
		filterBuffer = NULL;
	}
}

std::wstring EventHandler::check_selected_pdf_file_path(HWND hwndEditBox)
{
	if (hwndEditBox == NULL)
	{
		show_input_box_error_message();

		return L"";
	}

	LRESULT length = ::SendMessage(hwndEditBox, WM_GETTEXTLENGTH, 0, 0);
	wchar_t *buffer = new wchar_t[length + 1];

	if (buffer == NULL)
	{
		set_selected_pdf_file_path(L"", hwndEditBox);
		show_input_box_error_message();
		
		return L"";
	}

	::SendMessage(hwndEditBox, WM_GETTEXT, (WPARAM)(length + 1), (LPARAM)buffer);
	std::wstring selectedPDFFilePath = buffer;

	delete buffer;

	return selectedPDFFilePath;
}

void EventHandler::init_select_file_filter()
{
	std::wstring dialogFileFilter = this->localeMessage->get_i18n_message(L"app_diag_file_filter_text");
	int bufferSize = dialogFileFilter.size() + 8;

	filterBuffer = new wchar_t[bufferSize];

	if (filterBuffer != NULL)
	{
		::ZeroMemory(filterBuffer, bufferSize);
		::wcsncpy_s(filterBuffer, bufferSize, dialogFileFilter.c_str(), dialogFileFilter.size());
		filterBuffer[dialogFileFilter.size()] = L'\0';
		::wcsncpy_s(filterBuffer + dialogFileFilter.size() + 1, 7, L"*.pdf", 5);
		filterBuffer[dialogFileFilter.size() + 6] = L'\0';
		filterBuffer[dialogFileFilter.size() + 7] = L'\0';
		pdfFileName.lpstrFilter = filterBuffer;
	}
}

void EventHandler::init_select_file_data()
{
	static std::wstring dialogTitle = this->localeMessage->get_i18n_message(L"app_diag_caption_text");

	if (filterBuffer == NULL)
	{
		pdfFileName.lpstrFilter = L"PDF File (*.pdf)\0*.pdf\0\0";
	}
	else
	{
		pdfFileName.lpstrFilter = filterBuffer;
	}

	pdfFileName.lStructSize = sizeof(OPENFILENAME);
	pdfFileName.hwndOwner = hwndParent;
	pdfFileName.nFilterIndex = 1;
	pdfFileName.lpstrFile = pdfFilePath;
	pdfFileName.nMaxFile = sizeof(pdfFilePath);
	pdfFileName.lpstrInitialDir = NULL;
	pdfFileName.lpstrTitle = dialogTitle.c_str();
	pdfFileName.Flags = OFN_FILEMUSTEXIST | OFN_PATHMUSTEXIST | OFN_HIDEREADONLY | OFN_NOCHANGEDIR | OFN_EXPLORER | OFN_NONETWORKBUTTON;
}

void EventHandler::show_input_box_error_message()
{
	::MessageBox(hwndParent, localeMessage->get_i18n_message(L"app_error_file_path_text").c_str(), localeMessage->get_i18n_message(L"app_msgbox_error_caption_text").c_str(), MB_ICONSTOP | MB_OK);
}

void EventHandler::set_selected_pdf_file_path(const std::wstring& pdf_file_path, HWND hwndEditBox)
{
	if (pdf_file_path.size() >= MAX_PATH || hwndEditBox == NULL)
	{
		show_input_box_error_message();
	}

	::SendMessage(hwndEditBox, WM_SETTEXT, 0, (LPARAM)pdf_file_path.c_str());
}

bool EventHandler::handle_select_pdf_file_event(HWND hwndEditBox)
{
	if (hwndEditBox == NULL)
	{
		show_input_box_error_message();

		return false;
	}

	bool isSelectedPDFFile = ::GetOpenFileName(&pdfFileName);

	if (!isSelectedPDFFile)
	{
		return false;
	}

	set_selected_pdf_file_path(pdfFilePath, hwndEditBox);

	return true;
}

bool EventHandler::handle_export_annotation_event(HWND hwndEditBox)
{
	if (hwndEditBox == NULL)
	{
		show_input_box_error_message();

		return false;
	}

	std::wstring selectedPDFFilePath = check_selected_pdf_file_path(hwndEditBox);

	if (selectedPDFFilePath.empty())
	{
		::MessageBox(hwndParent, localeMessage->get_i18n_message(L"app_tips_no_file_text").c_str(), localeMessage->get_i18n_message(L"app_msgbox_tips_caption_text").c_str(), MB_ICONINFORMATION | MB_OK);
		return false;
	}

	selectedPDFFilePath = FileUtil::get_file_absolute_path(selectedPDFFilePath);

	bool isFileExists = FileUtil::check_file_exists(selectedPDFFilePath);

	if (!isFileExists)
	{
		::MessageBox(hwndParent, localeMessage->get_i18n_message(L"app_tips_file_exists_text").c_str(), localeMessage->get_i18n_message(L"app_msgbox_tips_caption_text").c_str(), MB_ICONINFORMATION | MB_OK);
		return false;
	}

	std::wstring fileMimeType = FileUtil::detect_file_mime_type(selectedPDFFilePath);

	if (fileMimeType.empty() || !fileMimeType._Equal(L"application/pdf"))
	{
		::MessageBox(hwndParent, localeMessage->get_i18n_message(L"app_tips_file_correct_text").c_str(), localeMessage->get_i18n_message(L"app_msgbox_tips_caption_text").c_str(), MB_ICONINFORMATION | MB_OK);
		return false;
	}

	XlsExportor xlsExportor(selectedPDFFilePath, localeMessage, hwndParent);
	std::wstring xlsExportFilePath = xlsExportor.get_default_export_path();

	xlsExportor.export_annotation(xlsExportFilePath);

	return true;
}