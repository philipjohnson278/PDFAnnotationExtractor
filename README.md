# PDF Annotation Extractor
A little tool for extracting annotation(s) contained in PDF file to .xls file.

## Installation
Tested with Visual Studio 2019, should work with Visual Studio 2019 and newer version. 
1. Clone code from https://gitlab.com/philipjohnson278/PDFAnnotationExtractor.git
2. Open PDFAnnotationExtractor.sln with Visual Studio, and build the project

## Dependencies
1. xlslib, http://xlslib.sourceforge.net
2. MuPDF, https://www.mupdf.com

## License
GNU General Public License