/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * PDFAnnotationExtractor is Copyright (c) 2020-2020 Nathan Yu.
 *
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * For commercial licensing, please contact nathan17724@gmail.com.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef UNICODE
#define UNICODE
#endif 

#include <Windows.h>

#include "ApplicationResource.h"
#include "i18nMessage.h"
#include "EventHandler.h"
#include "WindowLayout.h"

HWND g_hwndEditBox;
EventHandler *g_eventHandler;

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE, PWSTR pCmdLine, int nCmdShow)
{
    const wchar_t CLASS_NAME[] = L"PDF Annotation Extractor Window Class";
    WNDCLASS wc = {0};
    i18nMessage localeMessage;

    wc.lpfnWndProc = WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = CLASS_NAME;
    wc.hIcon = (HICON)::LoadImage(hInstance,  MAKEINTRESOURCE(IDI_APPLICATION_ICON), IMAGE_ICON, IDI_APPLICATION_ICON_WIDTH, IDI_APPLICATION_ICON_HEIGHT, LR_DEFAULTCOLOR | LR_SHARED | LR_DEFAULTSIZE | LR_LOADTRANSPARENT);
    wc.hbrBackground = (HBRUSH)(COLOR_APPWORKSPACE + 1);

    ::RegisterClass(&wc);

    HWND hwnd = ::CreateWindow(CLASS_NAME, localeMessage.get_i18n_message(L"app_caption_text").c_str(), (WS_OVERLAPPEDWINDOW ^ WS_THICKFRAME) & ~WS_MAXIMIZEBOX, CW_USEDEFAULT, CW_USEDEFAULT, 920, 260, NULL, NULL, hInstance, NULL);

    if (hwnd == NULL)
    {
        ::MessageBox(NULL, localeMessage.get_i18n_message(L"app_error_inititalization_text").c_str(), localeMessage.get_i18n_message(L"app_msgbox_error_caption_text").c_str(), MB_ICONSTOP | MB_OK);
        return 0;
    }

    WindowLayout windowLayout(&localeMessage, hwnd, hInstance);
    HWND hwndEditBox = windowLayout.create_input_box(100, 100, 500, 30);
    HWND hwndSelectButton = windowLayout.create_select_button(610, 100, 100, 30);
    HWND hwndExportButton = windowLayout.create_export_button(720, 100, 80, 30);

    if (hwndEditBox == NULL || hwndSelectButton == NULL || hwndExportButton == NULL)
    {
        ::MessageBox(NULL, localeMessage.get_i18n_message(L"app_error_inititalization_text").c_str(), localeMessage.get_i18n_message(L"app_msgbox_error_caption_text").c_str(), MB_ICONSTOP | MB_OK);
        return 0;
    }

    windowLayout.set_input_box_text_vertical_center(hwndEditBox);
    windowLayout.center_window_on_screen(hwnd);

    EventHandler eventHandler(&localeMessage, hwnd);

    g_hwndEditBox = hwndEditBox;
    g_eventHandler = &eventHandler;

    ::ShowWindow(hwnd, nCmdShow);
    ::SetFocus(hwndEditBox);

    MSG message = {0};
    
    while (::GetMessage(&message, NULL, 0, 0))
    {
        ::TranslateMessage(&message);
        ::DispatchMessage(&message);
    }

    ::DestroyWindow(hwndEditBox);
    ::DestroyWindow(hwndSelectButton);
    ::DestroyWindow(hwndExportButton);
    ::DestroyWindow(hwnd);

    return (int)message.wParam;
}

LRESULT CALLBACK WindowProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    int wmId;
    int wmEvent;

    switch (uMsg)
    {
        case WM_DESTROY:
        {
            ::PostQuitMessage(0);
            return 0;
        }

        case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = ::BeginPaint(hwnd, &ps);

            ::FillRect(hdc, &ps.rcPaint, (HBRUSH)(COLOR_WINDOW + 1));
            ::EndPaint(hwnd, &ps);
        
            return 0;
        }

        case WM_COMMAND:
        {
            wmId = LOWORD(wParam);
            wmEvent = HIWORD(wParam);

            switch (wmId)
            {
                case BTN_ID_SELECT_FILE:
                {
                    g_eventHandler->handle_select_pdf_file_event(g_hwndEditBox);
                    break;
                }

                case BTN_ID_EXPORT_FILE:
                {
                    g_eventHandler->handle_export_annotation_event(g_hwndEditBox);
                    break;
                }
            }
            
            return 0;
        }
    }

    return DefWindowProc(hwnd, uMsg, wParam, lParam);
}