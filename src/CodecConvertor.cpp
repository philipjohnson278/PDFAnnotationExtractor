/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * PDFAnnotationExtractor is Copyright (c) 2020-2020 Nathan Yu.
 *
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * For commercial licensing, please contact nathan17724@gmail.com.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <codecvt>
#include <locale>

#include "CodecConvertor.h"
#include "i18nMessage.h"

std::string CodecConvertor::wide_to_narrow(std::wstring const& wideString)
{
	int wcharSize = 4;
	std::string localeName = i18nMessage::get_ansi_locale_name();
	std::locale locale(localeName);
	const wchar_t *dataFrom = wideString.c_str();
	const wchar_t *dataFromEnd = wideString.c_str() + wideString.size();
	const wchar_t *dataFromNext = 0;
	char *dataTo = new char[(wideString.size() + 1) * wcharSize];
	char *dataToEnd = dataTo + (wideString.size() + 1) * wcharSize;
	char *dataToNext = 0;

	std::memset(dataTo, 0, (wideString.size() + 1) * wcharSize);
	typedef std::codecvt<wchar_t, char, mbstate_t> convertFacet;

	std::string ansi;
	mbstate_t outState = { 0 };
	std::codecvt_base::result convertResult = std::use_facet<convertFacet>(locale).out(outState, dataFrom, dataFromEnd, dataFromNext, dataTo, dataToEnd, dataToNext);

	if (convertResult == convertFacet::ok)
	{
		ansi = dataTo;
	}

	delete[] dataTo;

	return ansi;
}

std::wstring CodecConvertor::narrow_to_wide(std::string const& narrowString)
{
	std::string localeName = i18nMessage::get_ansi_locale_name();
	std::locale locale(localeName);
	const char *dataFrom = narrowString.c_str();
	const char *dataFromEnd = narrowString.c_str() + narrowString.size();
	const char *dataFromNext = 0;
	wchar_t *dataTo = new wchar_t[narrowString.size() + 1];
	wchar_t *dataToEnd = dataTo + narrowString.size() + 1;
	wchar_t *dataToNext = 0;

	std::wmemset(dataTo, 0, narrowString.size() + 1);
	typedef std::codecvt<wchar_t, char, mbstate_t> convertFacet;

	std::wstring wide;
	mbstate_t in_state = { 0, 0 };
	std::codecvt_base::result convertResult = std::use_facet<convertFacet>(locale).in(in_state, dataFrom, dataFromEnd, dataFromNext, dataTo, dataToEnd, dataToNext);

	if (convertResult == convertFacet::ok)
	{
		wide = dataTo;
	}

	delete[] dataTo;

	return wide;
}

std::string CodecConvertor::wstring_to_ansi(std::wstring const& wString)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter(std::string(""), std::wstring(L""));
	std::string ansi = converter.to_bytes(wString);

	return ansi;
}

std::wstring CodecConvertor::ansi_to_wstring(std::string const& ansiString)
{
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>> converter(std::string(""), std::wstring(L""));
	std::wstring wString = converter.from_bytes(ansiString);

	return wString;
}

std::wstring CodecConvertor::utf8_to_wstring(const std::string& utf8String)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> convertor;
	std::wstring wide = convertor.from_bytes(utf8String);

	return wide;
}

std::string CodecConvertor::wstring_to_utf8(const std::wstring& wString)
{
	std::wstring_convert<std::codecvt_utf8<wchar_t>> convertor;
	std::string utf8 = convertor.to_bytes(wString);

	return utf8;
}

std::wstring CodecConvertor::replace_trailing_to_windows_style(const std::wstring& targetWString, const std::wstring& oldTrailing)
{
	std::wstring windowsTrailing = L"\r\n";
	std::wstring replacedString = targetWString;
	std::wstring::size_type position = replacedString.find(oldTrailing);

	while (position != std::wstring::npos)
	{
		replacedString.replace(position, oldTrailing.size(), windowsTrailing);

		if (position + 1 < replacedString.size())
		{
			position = replacedString.find(oldTrailing, position + 1);
		}
		else
		{
			break;
		}
	}

	return replacedString;
}