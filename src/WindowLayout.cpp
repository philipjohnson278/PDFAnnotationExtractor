/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 *
 * PDFAnnotationExtractor is Copyright (c) 2020-2020 Nathan Yu.
 *
 * All Rights Reserved.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along
 * with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * For commercial licensing, please contact nathan17724@gmail.com.
 *
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#include <Windows.h>
#include <CommCtrl.h>
#include <RichEdit.h>

#include "WindowLayout.h"

WindowLayout::WindowLayout(const i18nMessage *localeMessage, const HWND hwndParent, const HINSTANCE hInstance)
{
	if (localeMessage == NULL)
	{
		::MessageBox(NULL, L"Application Initialization Error!", L"!!!ERROR!!!", MB_ICONSTOP | MB_OK);
		exit(0);
	}

	if (hwndParent == NULL || hInstance == NULL)
	{
		::MessageBox(NULL, ((i18nMessage*)localeMessage)->get_i18n_message(L"app_error_inititalization_text").c_str(), ((i18nMessage*)localeMessage)->get_i18n_message(L"app_msgbox_error_caption_text").c_str(), MB_ICONSTOP | MB_OK);
		exit(0);
	}

	this->hwndParent = (HWND)hwndParent;
	this->hInstance = (HINSTANCE)hInstance;
	this->hmoduleRichEdit = LoadLibrary(L"Msftedit.dll");
	this->localeMessage = (i18nMessage *)localeMessage;
}

WindowLayout::~WindowLayout()
{
	this->hwndParent = NULL;
	this->localeMessage = NULL;

	if (hmoduleRichEdit != NULL)
	{
		::FreeLibrary(hmoduleRichEdit);
	}
}

HWND WindowLayout::create_input_box(const int xPos, const int yPos, const int width, const int height)
{
	int x = xPos;
	int y = yPos;
	int w = width;
	int h = height;

	if (xPos < 0)
	{
		x = 40;
	}

	if (yPos < 0)
	{
		y = 40;
	}

	if (width <= 0)
	{
		w = 300;
	}

	if (height <= 0)
	{
		h = 30;
	}

	std::wstring editControlClassName = MSFTEDIT_CLASS;

	if (hmoduleRichEdit == NULL)
	{
		editControlClassName = WC_EDIT;
	}

	HWND hwndInputBox = ::CreateWindow(editControlClassName.c_str(), L"", WS_CHILD | WS_VISIBLE | WS_BORDER | ES_AUTOHSCROLL, x, y, w, h, hwndParent, (HMENU)BOX_ID_FILE_PATH, hInstance, NULL);

	return hwndInputBox;
}

HWND WindowLayout::create_select_button(const int xPos, const int yPos, const int width, const int height)
{
	int x = xPos;
	int y = yPos;
	int w = width;
	int h = height;

	if (xPos < 0)
	{
		x = 350;
	}

	if (yPos < 0)
	{
		y = 40;
	}

	if (width <= 0)
	{
		w = 100;
	}

	if (height <= 0)
	{
		h = 30;
	}

	HWND hwndSelectBtn = ::CreateWindow(WC_BUTTON, localeMessage->get_i18n_message(L"app_btn_choice_text").c_str(), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, x, y, w, h, hwndParent, (HMENU)BTN_ID_SELECT_FILE, hInstance, NULL);

	return hwndSelectBtn;
}

HWND WindowLayout::create_export_button(const int xPos, const int yPos, const int width, const int height)
{
	int x = xPos;
	int y = yPos;
	int w = width;
	int h = height;

	if (xPos < 0)
	{
		x = 460;
	}

	if (yPos < 0)
	{
		y = 40;
	}

	if (width <= 0)
	{
		w = 80;
	}

	if (height <= 0)
	{
		h = 30;
	}

	HWND hwndExportBtn = ::CreateWindow(WC_BUTTON, localeMessage->get_i18n_message(L"app_btn_export_text").c_str(), WS_CHILD | WS_VISIBLE | BS_PUSHBUTTON, x, y, w, h, hwndParent, (HMENU)BTN_ID_EXPORT_FILE, hInstance, NULL);

	return hwndExportBtn;
}

void WindowLayout::set_input_box_text_vertical_center(const HWND hwndEditBox)
{
	if (hwndEditBox == NULL)
	{
		return;
	}

	RECT rect = {0};
	LOGFONT logFont = {0};
	CHARFORMATA inputFormat = { 0 };
	HFONT hFont = (HFONT)::GetStockObject(DEFAULT_GUI_FONT);

	::GetObject(hFont, sizeof(LOGFONT), &logFont);
	logFont.lfWeight = 400;
	logFont.lfHeight = 16;
	inputFormat.cbSize = sizeof(CHARFORMATA);
	::DeleteObject(hFont);
	hFont = ::CreateFontIndirect(&logFont);
	
	::SendMessage(hwndEditBox, WM_SETFONT, (WPARAM)hFont, 0);
	::SendMessage(hwndEditBox, EM_GETCHARFORMAT, 0, (LPARAM)&inputFormat);
	::SendMessage(hwndEditBox, EM_GETRECT, 0, (LPARAM)&rect);
	
	rect.left += 1;
	rect.right -= 1;
	rect.top += (rect.bottom - rect.top - abs(logFont.lfHeight)) / 2 + 1;
	rect.bottom -= 1;
	inputFormat.dwMask = inputFormat.dwMask & ~CFM_BOLD;
	inputFormat.dwEffects = inputFormat.dwEffects & ~CFM_BOLD;
	::DeleteObject(hFont);

	::SendMessage(hwndEditBox, EM_SETRECT, 1, (LPARAM)&rect);
	::SendMessage(hwndEditBox, EM_SETTEXTMODE, TM_PLAINTEXT, 0);
	::SendMessage(hwndEditBox, EM_SETCHARFORMAT, 0, (LPARAM)&inputFormat);
}

void WindowLayout::center_window_on_screen(const HWND hwnd)
{
	if (hwnd == NULL)
	{
		return;
	}

	int screenWidth;
	int screenHeight;
	RECT rect = {0};
	
	screenWidth = ::GetSystemMetrics(SM_CXSCREEN);
	screenHeight = ::GetSystemMetrics(SM_CYSCREEN);
	::GetWindowRect(hwnd, &rect);
	
	long width = rect.right - rect.left;
	long height = rect.bottom - rect.top;
	
	rect.left = (screenWidth - width) / 2;
	rect.top = (screenHeight - height) / 2;

	::SetWindowPos(hwnd, HWND_TOPMOST, rect.left, rect.top, width, height, SWP_SHOWWINDOW | SWP_NOSIZE | SWP_NOZORDER);
	::UpdateWindow(hwnd);
	::BringWindowToTop(hwnd);
	::SetForegroundWindow(hwnd);
}